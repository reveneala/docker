FROM docker:18.09.1

RUN apk --no-cache add py-pip curl openssh-client git jq py2-dateutil py2-magic bash rsync lftp
#RUN pip install docker-compose
RUN printf "#!/bin/sh\ndocker run --rm -v \$PWD:/app composer:1.10.7 install --no-dev --ignore-platform-reqs --no-ansi --no-interaction --no-progress --no-scripts -a -d /app" > /usr/local/bin/composer-install
RUN printf "#!/bin/sh\ndocker run --rm -v \$PWD:/app composer:1.10.7 update --ignore-platform-reqs --no-ansi --no-interaction --no-progress -a -d /app" > /usr/local/bin/composer-update
RUN chmod +x /usr/local/bin/*